# Self-issued JWT bearer grant handler for the Connect2id server

Copyright (c) Connect2id Ltd., 2015 - 2022


README

Simple Connect2id server plugin for [handling self-issued JWT bearer 
grants](https://connect2id.com/products/server/docs/integration/jwt-bearer-grant-handler) 
(also see RFC 7523, sections 2.1, 3, 3.1 and 4).

* Requires [Connect2id server](https://connect2id.com/products/server) v9.0+.

* The scope of the access token scope is bound by the registered scope values
  for the requesting client (the `scope` client metadata field).

* Allows setting of the optional token `data` from selected client metadata
  fields.


## Configuration

The handler is configured from Java system properties or from properties in the
optional `/WEB-INF/selfIssuedJWTBearerHandler.properties` file. The Java system
properties have precedence. If no configuration properties are found the
handler is disabled.

* `op.grantHandler.selfIssuedJWTBearer.enable` -- If `true` enables the grant 
  handler. Disabled (`false`) by default.

* `op.grantHandler.selfIssuedJWTBearer.accessToken.lifetime` -- The access 
  token lifetime, in seconds.

* `op.grantHandler.selfIssuedJWTBearer.accessToken.encoding` -- The access 
  token encoding:
    * `IDENTIFIER` -- The access token is a secure identifier. The associated
      authorisation is looked up by a call to the Connect2id server token
      introspection endpoint.
    * `SELF_CONTAINED` -- Self-contained access token. The associated
      authorisation is encoded in the access token itself, as a signed and
      optionally encrypted JSON Web Token (JWT). Can also be looked up by a
      call to the Connect2id server token introspection endpoint.

* `op.grantHandler.selfIssuedJWTBearer.accessToken.encrypt` -- If `true` 
  enables additional encryption of self-contained (JWT-encoded) access tokens.

* `op.grantHandler.selfIssuedJWTBearer.accessToken.audienceList` --
  Optional audience for the access tokens, as comma and / or space separated
  list of values.

* `op.grantHandler.selfIssuedJWTBearer.accessToken.includeClientMetadataFields` 
  -- Names of client metadata fields to include in the optional access token
  `data` field, empty set if none. To specify a member within a field that is a
  JSON object member use dot (`.`) notation.

Example minimal configuration for issuing self-contained (JWT) access tokens
with a lifetime of 10 minutes (600 seconds):

```ini
op.grantHandler.selfIssuedJWTBearer.enable=true
op.grantHandler.selfIssuedJWTBearer.accessToken.lifetime=600
op.grantHandler.selfIssuedJWTBearer.accessToken.encoding=SELF_CONTAINED
```

Example configuration for issuing identifier-based access tokens which are
checked at the Connect2id server [token introspection
endpoint](https://connect2id.com/products/server/docs/api/token-introspection):

```ini
op.grantHandler.selfIssuedJWTBearer.enable=true
op.grantHandler.selfIssuedJWTBearer.accessToken.lifetime=600
op.grantHandler.selfIssuedJWTBearer.accessToken.encoding=IDENTIFIER
```

Example configuration which puts the registered client metadata fields
`software_id` and `data` -> `org_id` into the access token `data` field:

```ini
op.grantHandler.selfIssuedJWTBearer.enable=true
op.grantHandler.selfIssuedJWTBearer.accessToken.lifetime=600
op.grantHandler.selfIssuedJWTBearer.accessToken.encoding=IDENTIFIER
op.grantHandler.selfIssuedJWTBearer.accessToken.includeClientMetadataFields=software_id,data.org_id
```

## Change log

* 1.0 (2015‑10‑31)
    * First official release.

* 1.0.1 (2015‑11‑02)
    * Fixes SPI manifest.

* 1.1 (2021-03-03)
    * Adds op.grantHandler.selfIssuedJWTBearer.accessToken.includeClientMetadataFields
      config.
    * Lets op.grantHandler.selfIssuedJWTBearer.accessToken.audienceList apply 
      to identifier based access tokens, in addition to JWT-encoded tokens.
    * Upgrades dependencies.
    * Updates code for Java 11.

* 1.1.1 (2022-08-12)
  * Updates dependencies.

Questions or comments? Email support@connect2id.com
