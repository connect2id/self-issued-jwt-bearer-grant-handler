/**
 * Simple handler of self-issued JWT bearer grants. Issue of ID tokens is not
 * supported.
 */
package com.nimbusds.openid.connect.provider.spi.grants.jwt.selfissued.handler;