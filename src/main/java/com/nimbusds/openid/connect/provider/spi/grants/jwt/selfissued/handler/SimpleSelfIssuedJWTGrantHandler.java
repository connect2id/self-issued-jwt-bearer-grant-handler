package com.nimbusds.openid.connect.provider.spi.grants.jwt.selfissued.handler;


import java.util.HashSet;
import java.util.Properties;
import java.util.Set;

import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.oauth2.sdk.GeneralException;
import com.nimbusds.oauth2.sdk.GrantType;
import com.nimbusds.oauth2.sdk.OAuth2Error;
import com.nimbusds.oauth2.sdk.Scope;
import com.nimbusds.oauth2.sdk.id.ClientID;
import com.nimbusds.oauth2.sdk.id.Subject;
import com.nimbusds.oauth2.sdk.util.CollectionUtils;
import com.nimbusds.openid.connect.provider.spi.InitContext;
import com.nimbusds.openid.connect.provider.spi.grants.*;
import com.nimbusds.openid.connect.sdk.OIDCScopeValue;
import com.nimbusds.openid.connect.sdk.rp.OIDCClientMetadata;


/**
 * Self-issued JWT bearer grant handler. Implements the
 * {@link com.nimbusds.openid.connect.provider.spi.grants.SelfIssuedJWTGrantHandler}
 * Service Provider Interface (SPI) for plugging grant handlers into the
 * Connect2id server. The authorised scope is bounded by the registered scope
 * values in the OAuth 2.0 client metadata.
 *
 * <p>Note that self-issued (by the client) JWT bearer assertions are verified
 * (claims and JWS HMAC / signature) by the Connect2id server prior to calling
 * the handler. The handler only needs to determine the scope and optional
 * parameters for the access token. This handler doesn't support issue of ID
 * tokens.
 *
 * <p>Processing rules:
 *
 * <ol>
 *     <li>If no scope values are registered in the metadata for the requesting
 *         client, an {@code invalid_scope} OAuth 2.0 error is returned. See
 *         RFC 6749, section 5.2.
 *     <li>If no scope is explicitly requested (with the {@code scope} 
 *         parameter of the token request), the authorised scope defaults to  
 *         the registered scope values for the client.
 *     <li>If an explicit scope if requested (with the {@code scope} parameter
 *         of the token request), the authorised scope is reduced to those
 *         scope values for which the client is registered. See RFC 6749, 
 *         section 3.3.
 * </ol>
 *
 * <p>See RFC 7523 for more information on using JWT bearer assertions as OAuth
 * grants.
 *
 * <p>The Connect2id server also supports an SPI for handling JWT bearer
 * assertions issued by a third-party (such as a token service, or another
 * authorisation / identity provider).
 */
public class SimpleSelfIssuedJWTGrantHandler implements SelfIssuedJWTGrantHandler {


	/**
	 * The configuration file path.
	 */
	public static final String CONFIG_FILE_PATH = "/WEB-INF/selfIssuedJWTBearerHandler.properties";


	/**
	 * The configuration.
	 */
	private Configuration config;
	
	
	/**
	 * The client metadata filter.
	 */
	private ClientMetadataFilter clientMetadataFilter;
	
	
	/**
	 * Loads the configuration.
	 *
	 * @param initContext The initialisation context. Must not be
	 *                    {@code null}.
	 *
	 * @return The configuration.
	 *
	 * @throws Exception If loading failed.
	 */
	private static Configuration loadConfiguration(final InitContext initContext)
		throws Exception {
		
		var props = new Properties();
		
		var inputStream = initContext.getResourceAsStream(CONFIG_FILE_PATH);
		
		if (inputStream != null) {
			props.load(inputStream);
		}
		
		return new Configuration(props);
	}


	@Override
	public void init(final InitContext initContext)
		throws Exception {

		Loggers.MAIN.info("[SJH0000] Initializing self-issued JWT bearer grant handler...");
		config = loadConfiguration(initContext);
		config.log();
		
		if (config.enable) {
			clientMetadataFilter = new ClientMetadataFilter(config.accessToken.includeClientMetadataFields);
		}
	}


	/**
	 * Returns the configuration.
	 *
	 * @return The configuration.
	 */
	public Configuration getConfiguration() {

		return config;
	}


	@Override
	public GrantType getGrantType() {

		return GrantType.JWT_BEARER;
	}


	@Override
	public boolean isEnabled() {

		return config.enable;
	}


	@Override
	public SelfIssuedAssertionAuthorization processSelfIssuedGrant(final JWTClaimsSet jwtClaimsSet,
								       final Scope scope,
								       final ClientID clientID,
								       final OIDCClientMetadata clientMetadata)
		throws GeneralException {

		Loggers.TOKEN_ENDPOINT.debug("[SJH0002] Self-issued JWT bearer grant handler: Received request from client_id={} with scope={}", clientID, scope);

		final Scope registeredScopeValues = clientMetadata.getScope();

		if (CollectionUtils.isEmpty(registeredScopeValues)) {
			String msg = "No registered scopes for client";
			throw new GeneralException(msg, OAuth2Error.INVALID_SCOPE.setDescription(msg));
		}

		Scope authorizedScope;

		if (CollectionUtils.isEmpty(scope)) {
			// Implicit scope request, default to registered scope values
			authorizedScope = registeredScopeValues;
		} else {
			// Explicit scope request, discard any non-registered scope values
			authorizedScope = scope;
			authorizedScope.retainAll(registeredScopeValues);

			if (authorizedScope.isEmpty()) {
				// No scope values match
				String msg = "None of the requested scope values are permitted for this client";
				throw new GeneralException(msg, OAuth2Error.INVALID_SCOPE.setDescription(msg));
			}
		}

		// Compose the authorisation spec
		var sub = new Subject(jwtClaimsSet.getSubject());

		var accessTokenSpec = new AccessTokenSpec(
			config.accessToken.lifetime,
			config.accessToken.audienceList,
			config.accessToken.encoding,
			config.accessToken.encrypt);

		return new SelfIssuedAssertionAuthorization(
			sub,
			authorizedScope,
			accessTokenSpec,
			IDTokenSpec.NONE,
			resolveOpenIDClaims(authorizedScope),
			clientMetadataFilter.filter(clientMetadata) // Optional data
		);
	}


	/**
	 * Resolves the matching OpenID claims (if any) for the specified
	 * authorised scope.
	 *
	 * @param authorizedScope The authorised scope. May include OpenID
	 *                        standard scope values. Must not be
	 *                        {@code null}.
	 *
	 * @return The claims spec.
	 */
	public static ClaimsSpec resolveOpenIDClaims(final Scope authorizedScope) {

		Set<String> claimNames = new HashSet<>();

		if (authorizedScope.contains(OIDCScopeValue.EMAIL)) {
			claimNames.addAll(OIDCScopeValue.EMAIL.getClaimNames());
		}

		if (authorizedScope.contains(OIDCScopeValue.PHONE)) {
			claimNames.addAll(OIDCScopeValue.PHONE.getClaimNames());
		}

		if (authorizedScope.contains(OIDCScopeValue.PROFILE)) {
			claimNames.addAll(OIDCScopeValue.PROFILE.getClaimNames());
		}

		if (authorizedScope.contains(OIDCScopeValue.ADDRESS)) {
			claimNames.addAll(OIDCScopeValue.ADDRESS.getClaimNames());
		}

		if (! claimNames.isEmpty()) {
			return new ClaimsSpec(claimNames);
		} else {
			return ClaimsSpec.NONE;
		}
	}


	@Override
	public void shutdown() {

		// Nothing to do
		Loggers.MAIN.info("[SJH0003] Shut down self-issued JWT bearer grant handler");
	}
}
