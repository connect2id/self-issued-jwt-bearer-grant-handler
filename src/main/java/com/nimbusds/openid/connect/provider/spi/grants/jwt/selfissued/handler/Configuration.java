package com.nimbusds.openid.connect.provider.spi.grants.jwt.selfissued.handler;


import java.util.*;

import com.thetransactioncompany.util.PropertyParseException;
import com.thetransactioncompany.util.PropertyRetriever;

import com.nimbusds.common.config.ConfigurationException;
import com.nimbusds.common.config.LoggableConfiguration;
import com.nimbusds.oauth2.sdk.id.Audience;
import com.nimbusds.oauth2.sdk.token.TokenEncoding;
import com.nimbusds.oauth2.sdk.util.CollectionUtils;


/**
 * Self-issued JWT bearer grant handler configuration. It is typically derived 
 * from a Java key / value properties file. The configuration is stored as 
 * public fields which become immutable (final) after their initialisation.
 *
 * <p>Example configuration properties:
 *
 * <pre>
 * op.grantHandler.selfIssuedJWTBearer.enable=true
 * op.grantHandler.selfIssuedJWTBearer.accessToken.lifetime=3600
 * op.grantHandler.selfIssuedJWTBearer.accessToken.encoding=SELF_CONTAINED
 * op.grantHandler.selfIssuedJWTBearer.accessToken.encrypt=false
 * op.grantHandler.selfIssuedJWTBearer.accessToken.audienceList=http://s1.example.com http://s2.example.com
 * op.grantHandler.selfIssuedJWTBearer.accessToken.includeClientMetadataFields=software_id data.org_id
 * </pre>
 */
public final class Configuration implements LoggableConfiguration {


	/**
	 * The properties prefix.
	 */
	public static final String PREFIX = "op.grantHandler.selfIssuedJWTBearer.";


	/**
	 * Enables / disables the Self-issued JWT bearer grant handler.
	 *
	 * <p>Property key: [prefix]enable
	 */
	public final boolean enable;


	/**
	 * Access token settings for successfully authorised self-issued JWT
	 * bearer grants.
	 */
	public static class AccessToken implements LoggableConfiguration {


		/**
		 * The access token lifetime in seconds.
		 *
		 * <p>Property key: [prefix]lifetime
		 */
		public final long lifetime;


		/**
		 * The access token encoding.
		 *
		 * <p>Property key: [prefix]encoding
		 */
		public final TokenEncoding encoding;


		/**
		 * Enables / disables encryption of self-contained (JWT-
		 * encoded) access tokens.
		 *
		 * <p>Property key: [prefix]encrypt
		 */
		public final boolean encrypt;


		/**
		 * The audience list for self-contained access tokens,
		 * {@code null} if not specified or if the access token is
		 * identifier based.
		 *
		 * <p>Property key: [prefix]audienceList
		 */
		public final List<Audience> audienceList;
		
		
		/**
		 * Names of client metadata fields to include in the optional
		 * access token "data" field, empty set if none. To specify a
		 * member within a field that is a JSON object member use dot
		 * (.) notation.
		 *
		 * <p>Property key: [prefix]includeClientMetadataFields
		 */
		public final Set<String> includeClientMetadataFields;


		/**
		 * Creates a new access token configuration from the specified
		 * properties.
		 *
		 * @param prefix The properties prefix. Must not be
		 *               {@code null}.
		 * @param props  The properties. Must not be {@code null}.
		 *
		 * @throws PropertyParseException On a missing or invalid
		 *                                property.
		 */
		public AccessToken(final String prefix, final Properties props)
			throws PropertyParseException {

			var pr = new PropertyRetriever(props, true);

			lifetime = pr.getLong(prefix + "lifetime");

			encoding = pr.getEnum(prefix + "encoding", TokenEncoding.class);

			if (encoding.equals(TokenEncoding.SELF_CONTAINED)) {
				encrypt = pr.getBoolean(prefix + "encrypt");
			} else {
				encrypt = false;
			}
			
			String sList = pr.getOptString(prefix + "audienceList", null);
			
			if (sList != null && ! sList.trim().isEmpty()) {
				
				String[] sArray = sList.split("\\s+");
				
				List<Audience> list = new ArrayList<>(sArray.length);
				
				for (String aud: sArray) {
					list.add(new Audience(aud));
				}
				
				audienceList = Collections.unmodifiableList(list);
			} else {
				audienceList = null;
			}
			
			includeClientMetadataFields = new HashSet<>(pr.getOptStringList(prefix + "includeClientMetadataFields", Collections.emptyList()));
		}


		@Override
		public void log() {
			
			Loggers.MAIN.info("[SJH0010] Self-issued JWT bearer grant handler access token lifetime: {} s", lifetime);
			Loggers.MAIN.info("[SJH0011] Self-issued JWT bearer grant handler access token encoding: {}", encoding);

			if (encoding.equals(TokenEncoding.SELF_CONTAINED)) {
				Loggers.MAIN.info("[SJH0012] Self-issued JWT bearer grant handler access token encrypt: {}", encrypt);
			}

			if (audienceList != null) {
				Loggers.MAIN.info("[SJH0013] Self-issued JWT bearer grant handler access token audience: {}", audienceList);
			}
			
			if (CollectionUtils.isNotEmpty(includeClientMetadataFields)) {
				Loggers.MAIN.info("[SJH0015] Self-issued JWT bearer grant handler client metadata fields to include in access tokens: {}", includeClientMetadataFields);
			}
		}
	}


	/**
	 * The access token settings.
	 */
	public final AccessToken accessToken;


	/**
	 * Creates a new self-issued JWT bearer grant handler configuration
	 * from the specified properties.
	 *
	 * @param props The properties. Must not be {@code null}.
	 *
	 * @throws ConfigurationException On a missing or invalid property.
	 */
	public Configuration(final Properties props)
		throws ConfigurationException {

		var pr = new PropertyRetriever(props, true);

		try {
			enable = pr.getBoolean(PREFIX + "enable");
			accessToken = new AccessToken(PREFIX + "accessToken.", props);
		} catch (PropertyParseException e) {
			throw new ConfigurationException(e.getMessage() + ": Property: " + e.getPropertyKey());
		}
	}


	/**
	 * Logs the configuration details at INFO level. Properties that may
	 * adversely affect security are logged at WARN level.
	 */
	@Override
	public void log() {
		
		Loggers.MAIN.info("[SJH0014] Self-issued JWT bearer grant handler enabled: {}", enable);
		
		if (enable) {
			accessToken.log();
		}
	}
}
