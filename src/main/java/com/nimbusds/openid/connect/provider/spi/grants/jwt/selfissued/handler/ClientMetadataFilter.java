package com.nimbusds.openid.connect.provider.spi.grants.jwt.selfissued.handler;


import java.util.*;

import net.minidev.json.JSONObject;

import com.nimbusds.oauth2.sdk.client.ClientMetadata;
import com.nimbusds.oauth2.sdk.util.MapUtils;


/**
 * Client metadata filter.
 */
class ClientMetadataFilter {
	
	
	/**
	 * The maximum allowed path spec.
	 */
	public static final int MAX_PATH_DEPTH = 2;
	
	
	/**
	 * The client metadata field paths.
	 */
	private final Set<List<String>> paths;
	
	
	/**
	 * Creates a new client metadata filter.
	 *
	 * @param fieldIncludeSpecs The field spec obtained from the
	 *                          configuration.
	 */
	public ClientMetadataFilter(final Set<String> fieldIncludeSpecs) {
	
		var modPaths = new HashSet<List<String>>();
		
		fieldIncludeSpecs.forEach(spec -> {
			
			var path = parsePath(spec);
			
			if (path.size() > MAX_PATH_DEPTH) {
				throw new IllegalArgumentException("The maximum allowed field depth is " + MAX_PATH_DEPTH);
			}
			
			modPaths.add(path);
		});
		
		paths = Collections.unmodifiableSet(modPaths);
	}
	
	
	/**
	 * Parses a dot delimited path.
	 *
	 * @param pathSpec The path spec. Must not be {@code null}.
	 *
	 * @return The path.
	 */
	public static List<String> parsePath(final String pathSpec) {
		
		return Arrays.asList(pathSpec.split("\\."));
	}
	
	
	/**
	 * Returns the configured client metadata field paths.
	 *
	 * @return The field paths.
	 */
	public Set<List<String>> getFieldPaths() {
		return paths;
	}
	
	
	/**
	 * Filters the specified client metadata.
	 *
	 * @param clientMetadata The client metadata. Must not be {@code null}.
	 *
	 * @return The filtered fields, {@code null} if none.
	 */
	public JSONObject filter(final ClientMetadata clientMetadata) {
		
		if (getFieldPaths().isEmpty()) {
			return null;
		}
		
		JSONObject in = clientMetadata.toJSONObject();
		JSONObject out = new JSONObject();
		
		for (List<String> path: getFieldPaths()) {
			
			if (path.size() == 1) {
				
				// Simple case, e.g. "software_id"
				
				String fieldName = path.get(0);
				
				Object value = in.get(fieldName);
				
				if (value != null) {
					out.put(fieldName, value);
				}
				
			} else if (path.size() == 2) {
				
				// Path with 2 more components, e.g. "data.org_id"
				
				Object topLevelValue = in.get(path.get(0));
				
				if (! (topLevelValue instanceof JSONObject)) {
					continue; // skip
				}
				
				JSONObject topLevelJSONObject = (JSONObject) topLevelValue;
				
				String fieldName = path.get(1);
				
				Object value = topLevelJSONObject.get(fieldName);
				
				if (value != null) {
					out.put(fieldName, value);
				}
			}
		}
		
		if (MapUtils.isEmpty(out)) {
			return null;
		}
		
		return out;
	}
}
