package com.nimbusds.openid.connect.provider.spi.grants.jwt.selfissued.handler;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


/**
 * Log4j loggers.
 */
final class Loggers {


	/**
	 * The main logger. Records general configuration, startup, shutdown
	 * and system messages.
	 */
	public static final Logger MAIN = LogManager.getLogger("MAIN");
	
	
	/**
	 * The OAuth 2.0 token endpoint logger.
	 */
	public static final Logger TOKEN_ENDPOINT = LogManager.getLogger("TOKEN");
}
