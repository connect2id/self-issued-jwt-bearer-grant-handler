package com.nimbusds.openid.connect.provider.spi.grants.jwt.selfissued.handler;


import java.util.Collections;
import java.util.Date;

import static org.junit.Assert.*;

import com.thetransactioncompany.util.PropertyFilter;
import net.minidev.json.JSONObject;
import org.junit.After;
import org.junit.Test;

import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.oauth2.sdk.*;
import com.nimbusds.oauth2.sdk.id.ClientID;
import com.nimbusds.oauth2.sdk.id.SoftwareID;
import com.nimbusds.oauth2.sdk.token.TokenEncoding;
import com.nimbusds.openid.connect.provider.spi.grants.ClaimsSpec;
import com.nimbusds.openid.connect.provider.spi.grants.SelfIssuedAssertionAuthorization;
import com.nimbusds.openid.connect.sdk.OIDCScopeValue;
import com.nimbusds.openid.connect.sdk.rp.OIDCClientMetadata;


/**
 * Tests the self-issued JWT grant handler.
 */
public class SimpleSelfIssuedJWTGrantHandlerTest {


	private static JWTClaimsSet createVerifiedJWTClaimsSet() {

		return new JWTClaimsSet.Builder()
			.issuer("123")
			.subject("alice")
			.audience("https://c2id.com/token")
			.expirationTime(new Date(new Date().getTime() + 5 * 60 * 1000L))
			.build();
	}


	private static ClientID createClientID() {

		return new ClientID("123");
	}


	private static OIDCClientMetadata createClientMetadata() {

		var clientMetadata = new OIDCClientMetadata();
		clientMetadata.setScope(Scope.parse("read write"));
		clientMetadata.setGrantTypes(Collections.singleton(GrantType.JWT_BEARER));
		clientMetadata.setResponseTypes(Collections.<ResponseType>emptySet());
		clientMetadata.applyDefaults();
		return clientMetadata;
	}
	
	
	@After
	public void tearDown() {
		
		var filtered = PropertyFilter.filterWithPrefix(Configuration.PREFIX, System.getProperties());
		filtered.stringPropertyNames().forEach(System::clearProperty);
	}


	@Test
	public void testRequestWithScope()
		throws Exception {

		JWTClaimsSet verifiedJWTClaimsSet = createVerifiedJWTClaimsSet();
		Scope requestedScope = Scope.parse("read write admin");
		ClientID clientID = createClientID();
		OIDCClientMetadata clientMetadata = createClientMetadata();

		var handler = new SimpleSelfIssuedJWTGrantHandler();
		handler.init(new MockInitContext());
		assertEquals(GrantType.JWT_BEARER, handler.getGrantType());

		SelfIssuedAssertionAuthorization authzOut = handler.processSelfIssuedGrant(verifiedJWTClaimsSet, requestedScope, clientID, clientMetadata);
		assertEquals(Scope.parse("read write"), authzOut.getScope());
		assertEquals("http://s1.example.com", authzOut.getAudience().get(0).getValue());
		assertEquals(3600L, authzOut.getAccessTokenSpec().getLifetime());
		assertEquals(TokenEncoding.SELF_CONTAINED, authzOut.getAccessTokenSpec().getEncoding());
		assertFalse(authzOut.getAccessTokenSpec().encrypt());
	}


	@Test
	public void testRequestWithInvalidScope()
		throws Exception {

		JWTClaimsSet verifiedJWTClaimsSet = createVerifiedJWTClaimsSet();
		Scope requestedScope = Scope.parse("admin browse");
		ClientID clientID = createClientID();
		OIDCClientMetadata clientMetadata = createClientMetadata();

		var handler = new SimpleSelfIssuedJWTGrantHandler();
		handler.init(new MockInitContext());
		assertEquals(GrantType.JWT_BEARER, handler.getGrantType());

		try {
			handler.processSelfIssuedGrant(verifiedJWTClaimsSet, requestedScope, clientID, clientMetadata);
			fail();
		} catch (GeneralException e) {
			assertEquals(OAuth2Error.INVALID_SCOPE.getCode(), e.getErrorObject().getCode());
			assertEquals("None of the requested scope values are permitted for this client", e.getErrorObject().getDescription());
		}
	}


	@Test
	public void testRequestWithNoScope()
		throws Exception {

		JWTClaimsSet verifiedJWTClaimsSet = createVerifiedJWTClaimsSet();

		Scope requestedScope = null;

		ClientID clientID = createClientID();
		OIDCClientMetadata clientMetadata = createClientMetadata();

		var handler = new SimpleSelfIssuedJWTGrantHandler();
		handler.init(new MockInitContext());
		assertEquals(GrantType.JWT_BEARER, handler.getGrantType());

		SelfIssuedAssertionAuthorization authzOut = handler.processSelfIssuedGrant(verifiedJWTClaimsSet, requestedScope, clientID, clientMetadata);
		assertEquals(Scope.parse("read write"), authzOut.getScope());
		assertEquals("http://s1.example.com", authzOut.getAudience().get(0).getValue());
		assertEquals(3600L, authzOut.getAccessTokenSpec().getLifetime());
		assertEquals(TokenEncoding.SELF_CONTAINED, authzOut.getAccessTokenSpec().getEncoding());
		assertFalse(authzOut.getAccessTokenSpec().encrypt());
	}


	@Test
	public void testRequestWithEmptyScope()
		throws Exception {

		JWTClaimsSet verifiedJWTClaimsSet = createVerifiedJWTClaimsSet();
		Scope requestedScope = new Scope(); // empty scope
		ClientID clientID = createClientID();
		OIDCClientMetadata clientMetadata = createClientMetadata();

		var handler = new SimpleSelfIssuedJWTGrantHandler();
		handler.init(new MockInitContext());
		assertEquals(GrantType.JWT_BEARER, handler.getGrantType());

		SelfIssuedAssertionAuthorization authzOut = handler.processSelfIssuedGrant(verifiedJWTClaimsSet, requestedScope, clientID, clientMetadata);
		assertEquals(Scope.parse("read write"), authzOut.getScope());
		assertEquals("http://s1.example.com", authzOut.getAudience().get(0).getValue());
		assertEquals(3600L, authzOut.getAccessTokenSpec().getLifetime());
		assertEquals(TokenEncoding.SELF_CONTAINED, authzOut.getAccessTokenSpec().getEncoding());
		assertFalse(authzOut.getAccessTokenSpec().encrypt());
	}


	@Test
	public void testOverrideEnable()
		throws Exception {

		System.setProperty("op.grantHandler.selfIssuedJWTBearer.enable", "false");

		var handler = new SimpleSelfIssuedJWTGrantHandler();
		handler.init(new MockInitContext());

		assertFalse(handler.isEnabled());
	}


	@Test
	public void testOverrideTokenLifetime()
		throws Exception {

		System.setProperty("op.grantHandler.selfIssuedJWTBearer.accessToken.lifetime", "100");

		var handler = new SimpleSelfIssuedJWTGrantHandler();
		handler.init(new MockInitContext());

		assertEquals(100L, handler.getConfiguration().accessToken.lifetime);
	}
	
	
	@Test
	public void testOverrideEncoding()
		throws Exception {
		
		System.setProperty("op.grantHandler.selfIssuedJWTBearer.accessToken.encoding", "IDENTIFIER");
		
		JWTClaimsSet verifiedJWTClaimsSet = createVerifiedJWTClaimsSet();
		Scope requestedScope = Scope.parse("read write");
		ClientID clientID = createClientID();
		OIDCClientMetadata clientMetadata = createClientMetadata();
		clientMetadata.setScope(new Scope("read", "write"));
		clientMetadata.setSoftwareID(new SoftwareID("9ac9159d-30fc-482f-bd89-4bf9f32ddf78"));
		var data = new JSONObject();
		data.put("org_id", "123");
		clientMetadata.setCustomField("data", data);
		
		var handler = new SimpleSelfIssuedJWTGrantHandler();
		handler.init(new MockInitContext());
		assertEquals(GrantType.JWT_BEARER, handler.getGrantType());
		
		SelfIssuedAssertionAuthorization authzOut = handler.processSelfIssuedGrant(verifiedJWTClaimsSet, requestedScope, clientID, clientMetadata);
		assertEquals(Scope.parse("read write"), authzOut.getScope());
		assertEquals("http://s1.example.com", authzOut.getAudience().get(0).getValue());
		assertEquals(3600L, authzOut.getAccessTokenSpec().getLifetime());
		assertEquals(TokenEncoding.IDENTIFIER, authzOut.getAccessTokenSpec().getEncoding());
		assertFalse(authzOut.getAccessTokenSpec().encrypt());
		assertNull(authzOut.getData());
	}
	
	
	@Test
	public void testIncludeClientMetadataFields()
		throws Exception {
		
		System.setProperty("op.grantHandler.selfIssuedJWTBearer.accessToken.includeClientMetadataFields", "software_id data.org_id");
		
		JWTClaimsSet verifiedJWTClaimsSet = createVerifiedJWTClaimsSet();
		Scope requestedScope = Scope.parse("read write");
		ClientID clientID = createClientID();
		OIDCClientMetadata clientMetadata = createClientMetadata();
		clientMetadata.setScope(new Scope("read", "write"));
		clientMetadata.setSoftwareID(new SoftwareID("9ac9159d-30fc-482f-bd89-4bf9f32ddf78"));
		var data = new JSONObject();
		data.put("org_id", "123");
		clientMetadata.setCustomField("data", data);
		
		var handler = new SimpleSelfIssuedJWTGrantHandler();
		handler.init(new MockInitContext());
		assertEquals(GrantType.JWT_BEARER, handler.getGrantType());
		
		SelfIssuedAssertionAuthorization authzOut = handler.processSelfIssuedGrant(verifiedJWTClaimsSet, requestedScope, clientID, clientMetadata);
		assertEquals(Scope.parse("read write"), authzOut.getScope());
		assertEquals("http://s1.example.com", authzOut.getAudience().get(0).getValue());
		assertEquals(3600L, authzOut.getAccessTokenSpec().getLifetime());
		assertEquals(TokenEncoding.SELF_CONTAINED, authzOut.getAccessTokenSpec().getEncoding());
		assertFalse(authzOut.getAccessTokenSpec().encrypt());
		
		var expectedData = new JSONObject();
		expectedData.put("software_id", clientMetadata.getSoftwareID().getValue());
		expectedData.put("org_id", ((JSONObject)clientMetadata.getCustomField("data")).get("org_id"));
		assertEquals(expectedData, authzOut.getData());
	}


	@Test
	public void testResolveOpenIDClaimsNone() {

		assertEquals(ClaimsSpec.NONE, SimpleSelfIssuedJWTGrantHandler.resolveOpenIDClaims(new Scope()));
		assertEquals(ClaimsSpec.NONE, SimpleSelfIssuedJWTGrantHandler.resolveOpenIDClaims(new Scope("read")));
		assertEquals(ClaimsSpec.NONE, SimpleSelfIssuedJWTGrantHandler.resolveOpenIDClaims(new Scope("read", "write")));
		assertEquals(ClaimsSpec.NONE, SimpleSelfIssuedJWTGrantHandler.resolveOpenIDClaims(new Scope("openid")));
	}


	@Test
	public void testResolveClaims() {

		assertTrue(SimpleSelfIssuedJWTGrantHandler.resolveOpenIDClaims(new Scope("email")).getNames().containsAll(OIDCScopeValue.EMAIL.getClaimNames()));
		assertEquals(OIDCScopeValue.EMAIL.getClaimNames().size(), SimpleSelfIssuedJWTGrantHandler.resolveOpenIDClaims(new Scope("email")).getNames().size());

		assertTrue(SimpleSelfIssuedJWTGrantHandler.resolveOpenIDClaims(new Scope("phone")).getNames().containsAll(OIDCScopeValue.PHONE.getClaimNames()));
		assertEquals(OIDCScopeValue.PHONE.getClaimNames().size(), SimpleSelfIssuedJWTGrantHandler.resolveOpenIDClaims(new Scope("phone")).getNames().size());

		assertTrue(SimpleSelfIssuedJWTGrantHandler.resolveOpenIDClaims(new Scope("profile")).getNames().containsAll(OIDCScopeValue.PROFILE.getClaimNames()));
		assertEquals(OIDCScopeValue.PROFILE.getClaimNames().size(), SimpleSelfIssuedJWTGrantHandler.resolveOpenIDClaims(new Scope("profile")).getNames().size());

		assertTrue(SimpleSelfIssuedJWTGrantHandler.resolveOpenIDClaims(new Scope("address")).getNames().containsAll(OIDCScopeValue.ADDRESS.getClaimNames()));
		assertEquals(OIDCScopeValue.ADDRESS.getClaimNames().size(), SimpleSelfIssuedJWTGrantHandler.resolveOpenIDClaims(new Scope("address")).getNames().size());
	}
}
