package com.nimbusds.openid.connect.provider.spi.grants.jwt.selfissued.handler;


import java.util.Collections;
import java.util.List;
import java.util.Set;

import junit.framework.TestCase;
import net.minidev.json.JSONObject;

import com.nimbusds.oauth2.sdk.client.ClientMetadata;
import com.nimbusds.oauth2.sdk.id.SoftwareID;


public class ClientMetadataFilterTest extends TestCase {
	
	
	private static final ClientMetadata SAMPLE_CLIENT_METADATA;
	
	
	static {
		SAMPLE_CLIENT_METADATA = new ClientMetadata();
		SAMPLE_CLIENT_METADATA.setSoftwareID(new SoftwareID("9ac9159d-30fc-482f-bd89-4bf9f32ddf78"));
		var data = new JSONObject();
		data.put("org_id", "123");
		SAMPLE_CLIENT_METADATA.setCustomField("data", data);
	}


	public void testParsePathSpec() {
		
		assertEquals(List.of(""), ClientMetadataFilter.parsePath(""));
		assertEquals(List.of("software_id"), ClientMetadataFilter.parsePath("software_id"));
		assertEquals(List.of("data", "org_id"), ClientMetadataFilter.parsePath("data.org_id"));
	}
	
	
	public void testEmpty() {
		
		var filter = new ClientMetadataFilter(Collections.emptySet());
		
		assertTrue(filter.getFieldPaths().isEmpty());
		
		assertNull(filter.filter(SAMPLE_CLIENT_METADATA));
	}
	
	
	public void testFilter_twoFields() {
		
		var filter = new ClientMetadataFilter(Set.of("software_id", "data.org_id"));
		
		assertEquals(Set.of(List.of("software_id"), List.of("data", "org_id")), filter.getFieldPaths());
		
		var expected = new JSONObject();
		expected.put("software_id", SAMPLE_CLIENT_METADATA.getSoftwareID().getValue());
		expected.put("org_id", ((JSONObject)SAMPLE_CLIENT_METADATA.getCustomField("data")).get("org_id"));
		
		assertEquals(expected, filter.filter(SAMPLE_CLIENT_METADATA));
	}
	
	
	public void testFilter_oneField() {
		
		var filter = new ClientMetadataFilter(Set.of("data.org_id"));
		
		assertEquals(Set.of(List.of("data", "org_id")), filter.getFieldPaths());
		
		var expected = new JSONObject();
		expected.put("org_id", ((JSONObject)SAMPLE_CLIENT_METADATA.getCustomField("data")).get("org_id"));
		
		assertEquals(expected, filter.filter(SAMPLE_CLIENT_METADATA));
	}
	
	
	public void testPathTooDeep() {
		
		assertEquals(2, ClientMetadataFilter.MAX_PATH_DEPTH);
		
		try {
			new ClientMetadataFilter(Set.of("data.org.id"));
			fail();
		} catch (IllegalArgumentException e) {
			assertEquals("The maximum allowed field depth is " + ClientMetadataFilter.MAX_PATH_DEPTH, e.getMessage());
		}
	}
}
