package com.nimbusds.openid.connect.provider.spi.grants.jwt.selfissued.handler;


import java.util.Properties;
import java.util.Set;

import com.nimbusds.common.config.ConfigurationException;
import com.nimbusds.oauth2.sdk.token.TokenEncoding;
import junit.framework.TestCase;


/**
 * Tests the configuration class.
 */
public class ConfigurationTest extends TestCase {


	public void testConstructFromProperties() {

		var props = new Properties();
		props.setProperty("op.grantHandler.selfIssuedJWTBearer.enable", "true");
		props.setProperty("op.grantHandler.selfIssuedJWTBearer.accessToken.lifetime", "3600");
		props.setProperty("op.grantHandler.selfIssuedJWTBearer.accessToken.encoding", "SELF_CONTAINED");
		props.setProperty("op.grantHandler.selfIssuedJWTBearer.accessToken.encrypt", "false");
		props.setProperty("op.grantHandler.selfIssuedJWTBearer.accessToken.audienceList", "");

		var config = new Configuration(props);

		config.log();

		assertTrue(config.enable);
		assertEquals(3600L, config.accessToken.lifetime);
		assertEquals(TokenEncoding.SELF_CONTAINED, config.accessToken.encoding);
		assertFalse(config.accessToken.encrypt);
		assertNull(config.accessToken.audienceList);
		assertTrue(config.accessToken.includeClientMetadataFields.isEmpty());
	}


	public void testConstructFromPropertiesWithAudienceList() {

		var props = new Properties();
		props.setProperty("op.grantHandler.selfIssuedJWTBearer.enable", "true");
		props.setProperty("op.grantHandler.selfIssuedJWTBearer.accessToken.lifetime", "3600");
		props.setProperty("op.grantHandler.selfIssuedJWTBearer.accessToken.encoding", "SELF_CONTAINED");
		props.setProperty("op.grantHandler.selfIssuedJWTBearer.accessToken.encrypt", "true");
		props.setProperty("op.grantHandler.selfIssuedJWTBearer.accessToken.audienceList", "http://s1.example.com http://s2.example.com");

		var config = new Configuration(props);

		config.log();

		assertTrue(config.enable);
		assertEquals(3600L, config.accessToken.lifetime);
		assertEquals(TokenEncoding.SELF_CONTAINED, config.accessToken.encoding);
		assertTrue(config.accessToken.encrypt);
		assertEquals("http://s1.example.com", config.accessToken.audienceList.get(0).getValue());
		assertEquals("http://s2.example.com", config.accessToken.audienceList.get(1).getValue());
		assertEquals(2, config.accessToken.audienceList.size());
		assertTrue(config.accessToken.includeClientMetadataFields.isEmpty());
	}
	
	
	public void testConstructFromPropertiesWithClientMetadataFields() {
		
		var props = new Properties();
		props.setProperty("op.grantHandler.selfIssuedJWTBearer.enable", "true");
		props.setProperty("op.grantHandler.selfIssuedJWTBearer.accessToken.lifetime", "3600");
		props.setProperty("op.grantHandler.selfIssuedJWTBearer.accessToken.encoding", "SELF_CONTAINED");
		props.setProperty("op.grantHandler.selfIssuedJWTBearer.accessToken.encrypt", "false");
		props.setProperty("op.grantHandler.selfIssuedJWTBearer.accessToken.audienceList", "");
		props.setProperty("op.grantHandler.selfIssuedJWTBearer.accessToken.includeClientMetadataFields", "software_id data.org_id");
		
		var config = new Configuration(props);
		
		config.log();
		
		assertTrue(config.enable);
		assertEquals(3600L, config.accessToken.lifetime);
		assertEquals(TokenEncoding.SELF_CONTAINED, config.accessToken.encoding);
		assertFalse(config.accessToken.encrypt);
		assertNull(config.accessToken.audienceList);
		assertEquals(Set.of("software_id", "data.org_id"), config.accessToken.includeClientMetadataFields);
	}


	public void testConstructFromPropertiesWithIdentifierBasedAccessToken() {

		var props = new Properties();
		props.setProperty("op.grantHandler.selfIssuedJWTBearer.enable", "true");
		props.setProperty("op.grantHandler.selfIssuedJWTBearer.accessToken.lifetime", "3600");
		props.setProperty("op.grantHandler.selfIssuedJWTBearer.accessToken.encoding", "IDENTIFIER");
		props.setProperty("op.grantHandler.selfIssuedJWTBearer.accessToken.encrypt", "true");
		props.setProperty("op.grantHandler.selfIssuedJWTBearer.accessToken.audienceList", "http://s1.example.com http://s2.example.com");

		var config = new Configuration(props);

		config.log();

		assertTrue(config.enable);
		assertEquals(3600L, config.accessToken.lifetime);
		assertEquals(TokenEncoding.IDENTIFIER, config.accessToken.encoding);
		assertFalse(config.accessToken.encrypt);
		assertEquals("http://s1.example.com", config.accessToken.audienceList.get(0).getValue());
		assertEquals("http://s2.example.com", config.accessToken.audienceList.get(1).getValue());
		assertEquals(2, config.accessToken.audienceList.size());
		assertTrue(config.accessToken.includeClientMetadataFields.isEmpty());
	}


	public void testInvalid() {

		try {
			new Configuration(new Properties());
			fail();
		} catch (ConfigurationException e) {
			// ok
		}
	}
}