package com.nimbusds.openid.connect.provider.spi.grants.jwt.selfissued.handler;


import java.io.File;
import java.io.IOException;

import com.nimbusds.openid.connect.provider.spi.grants.SelfIssuedJWTGrantHandler;
import junit.framework.TestCase;
import org.apache.commons.io.FileUtils;


/**
 * Tests the SPI manifest.
 */
public class SPIManifestTest extends TestCase {
	

	public void testManifest()
		throws IOException {

		String spiCanonicalName = SelfIssuedJWTGrantHandler.class.getCanonicalName();
		File spiManifestFile = new File("src/main/resources/META-INF/services/" + spiCanonicalName);

		String content = FileUtils.readFileToString(spiManifestFile, "UTF-8");

		String spiImplCanonicalName = SimpleSelfIssuedJWTGrantHandler.class.getCanonicalName();

		assertTrue(content.contains(spiImplCanonicalName));
	}
}
