package com.nimbusds.openid.connect.provider.spi.grants.jwt.selfissued.handler;


import java.io.*;
import java.net.URI;
import java.util.Properties;

import com.nimbusds.oauth2.sdk.id.Issuer;
import com.nimbusds.openid.connect.provider.spi.InitContext;
import com.nimbusds.openid.connect.provider.spi.ServiceContext;


/**
 * Mock initialisation context.
 */
class MockInitContext implements InitContext {


	@Override
	public InputStream getResourceAsStream(final String path) {

		var props = new Properties();
		props.setProperty("op.grantHandler.selfIssuedJWTBearer.enable", "true");
		props.setProperty("op.grantHandler.selfIssuedJWTBearer.accessToken.lifetime", "3600");
		props.setProperty("op.grantHandler.selfIssuedJWTBearer.accessToken.encoding", "SELF_CONTAINED");
		props.setProperty("op.grantHandler.selfIssuedJWTBearer.accessToken.encrypt", "false");
		props.setProperty("op.grantHandler.selfIssuedJWTBearer.accessToken.audienceList", "http://s1.example.com");

		var os = new ByteArrayOutputStream();

		try {
			props.store(os, null);

		} catch (IOException e) {
			throw new RuntimeException(e.getMessage(), e);
		}

		return new ByteArrayInputStream(os.toByteArray());
	}


	@Override
	public Issuer getOPIssuer() {
		return null; // not required
	}


	@Override
	public URI getTokenEndpointURI() {
		return null; // not required
	}


	@Override
	public ServiceContext getServiceContext() {
		return null; // not required
	}
	
	
	@Override
	public javax.servlet.ServletContext getServletContext() {
		return null;
	}
	
	
	@Override
	public org.infinispan.manager.EmbeddedCacheManager getInfinispanCacheManager() {
		return null;
	}
	
	
	@Override
	public Issuer getIssuer() {
		return null;
	}
}
